package dawson;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }
    @Test
    public void testingEcho() {
        assertEquals("If it fails, this message should show up!",5, dawson.App.echo(5)); // I couldnt add the optional message //
    }
    @Test
    public void testingOneMore() {
        assertEquals("Hmm, maybe there is a bug here!",6, dawson.App.oneMore(5)); // Couldnt add it here either //
    }
}
